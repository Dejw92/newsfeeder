package agh.tai.newsfeeder.persistence.interfaces;

import agh.tai.newsfeeder.Comment;
import agh.tai.newsfeeder.event.Event;

import java.util.List;

public interface EventRepository {

    void save( Event event );

    void delete( String urlname );

    Event get( String urlname );

    List<Event> get(  );

    void deleteComment(String urlName, String commentId );

    void saveOrUpdateComment(String urlname, Comment comment);
}

