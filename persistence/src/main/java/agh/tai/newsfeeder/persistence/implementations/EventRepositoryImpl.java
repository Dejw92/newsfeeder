package agh.tai.newsfeeder.persistence.implementations;

import agh.tai.newsfeeder.Comment;
import agh.tai.newsfeeder.event.Event;
import agh.tai.newsfeeder.persistence.interfaces.EventRepository;
import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventRepositoryImpl implements EventRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void save(Event event) {
        delete(event.getUrlname());
        mongoTemplate.save(event);
    }

    @Override
    public void delete( String urlname ) {

        final BasicDBObject urlnameQuery = new BasicDBObject( "urlname", urlname );
        mongoTemplate.remove(new BasicQuery(urlnameQuery),Event.class, "events");
    }

    @Override
    public Event get( String urlname ) {

        final BasicDBObject urlnameQuery = new BasicDBObject( "urlname", urlname );
        BasicQuery query = new BasicQuery( urlnameQuery );

        return mongoTemplate.findOne( query, Event.class );
    }

    @Override
    public List<Event> get() {
        return mongoTemplate.findAll( Event.class );
    }

    @Override
    public void deleteComment(String urlname, String commentId) {
        Criteria criteria = new Criteria().andOperator(Criteria.where("urlname").is(urlname));
        Update update = new Update().pull("comments",new Query(Criteria.where("_id").is(commentId)));
        mongoTemplate.updateFirst(new Query(criteria),update,Event.class);
    }

    @Override
    public void saveOrUpdateComment(String urlname, Comment comment) {
        deleteComment(urlname, comment.getId());
        Update update = new Update().addToSet("comments", comment);
        mongoTemplate.updateFirst(new Query(Criteria.where("urlname").is(urlname)),update,Event.class);
    }

}
