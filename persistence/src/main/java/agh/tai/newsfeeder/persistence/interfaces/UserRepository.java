package agh.tai.newsfeeder.persistence.interfaces;

import agh.tai.newsfeeder.User;

/**
 * Created by Dawid Pawlak.
 */
public interface UserRepository {

    void save(User user);

    User get(String login);
}
