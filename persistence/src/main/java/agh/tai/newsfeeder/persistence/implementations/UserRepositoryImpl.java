package agh.tai.newsfeeder.persistence.implementations;

import agh.tai.newsfeeder.User;
import agh.tai.newsfeeder.persistence.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Dawid Pawlak.
 */

@Repository
public class UserRepositoryImpl implements UserRepository {


    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void save(User user) {
        mongoTemplate.save(user);
    }

    @Override
    public User get(String login) {
        Query query = new Query();
        query.addCriteria(Criteria.where("login").in(login));
        return mongoTemplate.findOne(query,User.class);
    }
}
