package controllers;

import agh.tai.newsfeeder.persistence.interfaces.EventRepository;
import agh.tai.newsfeeder.service.assembler.EventAssembler;
import agh.tai.newsfeeder.service.dtos.UserDTO;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import agh.tai.newsfeeder.service.meetup_client.MeetupClient;
import agh.tai.newsfeeder.service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Dawid Pawlak.
 */



@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="users/save", method= RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Boolean> saveUser(@RequestBody UserDTO userDTO) {
        userService.save(userDTO);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

}
