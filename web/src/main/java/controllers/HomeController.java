package controllers;


import agh.tai.newsfeeder.service.dtos.CurrentUserDetailsDTO;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import agh.tai.newsfeeder.service.services.GroupService;
import agh.tai.newsfeeder.service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    private final String ROLE_ALLOWED_TO_CREATE_EVENTS = "ROLE_CREATOR";

    @RequestMapping(value="/")
    public ModelAndView home() {
        ModelAndView mav = new ModelAndView("index");
        Map<String, List<EventDTO>> map = groupService.getGroupComponents();

        CurrentUserDetailsDTO currentUserDetailsDTO = userService.getCurrentlyLoggedUser();

        boolean canAddEvent = false;
        for(String role : currentUserDetailsDTO.getRoles()) {
            canAddEvent = canAddEvent || ROLE_ALLOWED_TO_CREATE_EVENTS.equals(role);
        }
        mav.addObject("canAddEvent", canAddEvent);
        mav.addObject("map", map);
        return mav;
    }

}
