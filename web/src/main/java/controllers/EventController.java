package controllers;

import agh.tai.newsfeeder.persistence.interfaces.EventRepository;
import agh.tai.newsfeeder.service.assembler.CommentAssembler;
import agh.tai.newsfeeder.service.assembler.EventAssembler;
import agh.tai.newsfeeder.service.dtos.CommentDTO;
import agh.tai.newsfeeder.service.dtos.CurrentUserDetailsDTO;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import agh.tai.newsfeeder.service.meetup_client.MeetupClient;
import agh.tai.newsfeeder.service.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Controller
public class EventController {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/events/create", method = RequestMethod.POST)
    public ModelAndView save(@RequestBody String urlname) throws IOException {
        urlname = urlname.split("=")[1];

        MeetupClient instance = new MeetupClient();
        Optional<EventDTO> optionalEvent = instance.getEvent(urlname);

        if( optionalEvent.isPresent() ) {
            EventDTO event = optionalEvent.get();

            event.setUrlname(urlname);

            eventRepository.save(EventAssembler.convert(event));
        }

        ModelAndView mav = new ModelAndView("redirect:/");
        mav.addObject("invaildUrlNameMessage", "Specified urlname is not found");
        return mav;
    }

    @RequestMapping(value = "/events/{urlname}", method = RequestMethod.GET)
    public ModelAndView get(@PathVariable String urlname) {

        ModelAndView mav = new ModelAndView("event");
        EventDTO eventDTO = EventAssembler.convert(eventRepository.get(urlname));
        mav.addObject("event", eventDTO);
        mav.addObject( "comment", new CommentDTO() );
        return mav;
    }

    @RequestMapping(value = "/events/{urlname}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable String urlname) {
        eventRepository.delete(urlname);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @RequestMapping(value = "/events/{urlname}/comment", method = RequestMethod.POST)
    public ModelAndView addOrUpdateComment(@PathVariable String urlname, @RequestBody String content) {

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent(content.split("=")[1]);
        CurrentUserDetailsDTO user = userService.getCurrentlyLoggedUser();
        if(StringUtils.isBlank(commentDTO.getId())) {
            commentDTO.setId(UUID.randomUUID().toString());
        }
        commentDTO.setAuthor(user.getLogin());
        commentDTO.setTime(DateTime.now());
        eventRepository.saveOrUpdateComment(urlname, CommentAssembler.convert(commentDTO));

        return new ModelAndView("redirect:/events/" + urlname);
    }

    @RequestMapping(value = "/events/{urlname}/comment/{commentId}", method = RequestMethod.DELETE, consumes = "application/json")
    public ResponseEntity<Boolean> deleteComment(@PathVariable String urlname, @PathVariable String commentId) {
        eventRepository.deleteComment(urlname, commentId);

        return new ResponseEntity<>(true, HttpStatus.OK);
    }

}
