<#import "spring.ftl" as spring />
<#import "navigation.ftl" as navigation/>


<#macro comment author date content>
<hr>
<div class="row">
    <div class="col-md-12">
        <b>${author}</b>
        <span class="pull-right">${date} </span>
        <p>${content}</p>
    </div>
</div>
<hr>

</#macro>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/event-item.css'/>" />
    <script src="<@spring.url '/js/jquery.js'/>"></script>
    <script src="<@spring.url '/js/bootstrap.min.js'/>"></script>
</head>

<body>

<@navigation.navigation/>

<div class="container">

    <div class="row">

        <div class="col-md-9">

            <div class="thumbnail">
                <img class="img-responsive" src="http://placehold.it/800x300" alt="">
                <div class="caption-full">
                    <h4><a href="#">${event.name}</a>
                    </h4>
                    <p>
                        ${event.description}
                    </p>
                </div>
            </div>

            <div class="well">
                <input type="text" id="urlnameInput" style="display: none">

                <form id="commentsForm" action="/events/${event.urlname}/comment" method="post">
                    <input name="content" type="text" class="form-control" id="formContent" placeholder="Text input">
                    <button type="submit" class="btn btn-primary" id="submitButton" style="margin-top:5px">Submit</button>
                </form>

                <#list event.comments as c >
                    <@comment "${c.author!}" "${c.time!}" "${c.content!}" />
                </#list>

            </div>
        </div>
    </div>

</div>
</body>

</html>