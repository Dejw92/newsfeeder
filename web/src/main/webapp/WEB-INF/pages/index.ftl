<#import "spring.ftl" as spring />
<#import "navigation.ftl" as navigation/>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/event-item.css'/>"/>
</head>

<body>

<@navigation.navigation/>

<#macro eventRow details urlname="#">
<a href="./events/${urlname}" class="list-group-item">
    <h4 class="list-group-item-heading">
        ${details}
    </h4>
</a>
</#macro>


<div class="container">
    <div class="row">
        <#list map?keys as groupName>
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    <h4 class="list-group-item-heading">
                        ${groupName}
                    </h4>
                </a>
                <#list map[groupName] as event>
                    <@eventRow details="${event.name}" urlname="${event.urlname}"/>
                </#list>
            </div>
        </#list>
    </div>
    <#if canAddEvent>
        <div class="row well">
            <p>Add event : </p>
            <p>${invaildUrlNameMessage!}</p>
            <form id="commentsForm" action="/events/create" method="post">
                <input name="urlname" type="text" class="form-control" id="urlname" placeholder="URL NAME">
                <button type="submit" class="btn btn-primary" id="submitButton" style="margin-top:5px">Submit</button>
            </form>
        </div>
    </#if>
</div>

<script src="<@spring.url '/js/jquery.js'/>"></script>
<script src="<@spring.url '/js/bootstrap.min.js'/>"></script>

</body>

</html>