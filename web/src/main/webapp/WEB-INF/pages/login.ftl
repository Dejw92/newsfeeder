<#import "spring.ftl" as spring />
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Sign in</title>

    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/bootstrap.min.css'/>"/>
</head>
<body>
<div class="container">
    <div class="row form-row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Login</strong></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" action="/login" method="post">
                        <div class="alert alert-error">
                            <p>${errorMessage!}</p>
                        </div>
                        <div class="form-group">
                            <label for="login" class="col-sm-3 control-label">Login</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="login" placeholder="Login" required="" type="text" name="username" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="password" placeholder="Password" required="" type="password" name="password" value=""/>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-4 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">Sign in</button>
                                <button type="button" class="btn btn-success btn-sm" onclick="window.location='register';">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>