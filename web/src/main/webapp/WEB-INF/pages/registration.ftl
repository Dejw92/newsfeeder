<#import "spring.ftl" as spring />

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Register</title>

    <link rel="stylesheet" type="text/css" href="<@spring.url '/css/bootstrap.min.css'/>"/>

    <script src="<@spring.url '/js/jquery.js'/>"></script>
    <script>
        $(document).ready(function() {
            $("#registrationForm").submit(function(event) {

                var frm = $("#registrationForm");
                var formData = frm.serializeArray();
                var data = {};
                $.map(formData, function (obj) {
                    data[obj['name']] = obj['value'];
                });
                data = JSON.stringify(data);

                event.preventDefault();

                $.ajax({
                    url: "/register",
                    data: data,
                    type: "POST",
                    contentType: "application/json",
                    success: function(data) {
                        if(data) {
                            window.location.href="login";
                        }
                    }
                });
            });
        });

    </script>


</head>
<body>
<div class="container">
    <div class="row form-row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Registration</strong>

                </div>
                <div class="panel-body">
                    <form id="registrationForm" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="login" class="col-sm-3 control-label">Login</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="login" placeholder="Login" required="" type="text" name="login" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login" class="col-sm-3 control-label">First name</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="firstName" placeholder="First name" required="" type="text" name="firstName" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login" class="col-sm-3 control-label">Last name</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="lastName" placeholder="Last name" required="" type="text" name="lastName" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role" class="col-sm-3 control-label">Role</label>
                            <div class="col-sm-9">
                                <select name="role">
                                    <option>CREATOR</option>
                                    <option>COMMENTER</option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="password" placeholder="Password" required="" type="password" name="password" value=""/>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>