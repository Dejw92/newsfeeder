package agh.tai.newsfeeder.event;

import agh.tai.newsfeeder.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Venue {

    private String name;
    private String country;
    private String city;
    private String address_1;

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry( String country ) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1( String address_1 ) {
        this.address_1 = address_1;
    }
}
