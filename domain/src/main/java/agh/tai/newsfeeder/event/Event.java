package agh.tai.newsfeeder.event;

import agh.tai.newsfeeder.BaseEntity;
import agh.tai.newsfeeder.Comment;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


@Document(collection = "events")
public class Event extends BaseEntity {

    private String name;
    private long time;
    private String event_url;
    private String urlname;
    private String status;
    private int yes_rsvp_count;
    private String description;
    private Group group;
    private Venue venue;

    private List<Comment> comments = new LinkedList<>();

    public Event() {
    }

    public Event(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Event(String name, String description, List<Comment> comments) {
        this(name, description);
        this.comments = comments;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname( String urlname ) {
        this.urlname = urlname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments( List<Comment> comments ) {
        this.comments = comments;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime( long time ) {
        this.time = time;
    }

    public String getEvent_url() {
        return event_url;
    }

    public void setEvent_url( String event_url ) {
        this.event_url = event_url;
    }

    public int getYes_rsvp_count() {
        return yes_rsvp_count;
    }

    public void setYes_rsvp_count( int yes_rsvp_count ) {
        this.yes_rsvp_count = yes_rsvp_count;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup( Group group ) {
        this.group = group;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue( Venue venue ) {
        this.venue = venue;
    }
}

