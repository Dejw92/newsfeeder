package agh.tai.newsfeeder.event;

import agh.tai.newsfeeder.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Group {

    private String name;
    private String urlname;

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname( String urlname ) {
        this.urlname = urlname;
    }
}
