package agh.tai.newsfeeder;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
public class Comment {

    private String id;

    private String content;

    private DateTime time;

    private String author;

    public Comment() {
    }

    public Comment(String id, String content, String author,DateTime time) {
        this.id = id;
        this.content = content;
        this.time = time;
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public DateTime getTime() {
        return time;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }


}
