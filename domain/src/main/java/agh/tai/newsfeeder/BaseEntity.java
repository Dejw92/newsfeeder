package agh.tai.newsfeeder;

import org.springframework.data.annotation.Id;
/**
 * Created by Dawid Pawlak.
 */

public abstract class BaseEntity {

    @Id
    private String _id;

    public BaseEntity() {
    }

    public String getId() {
        return _id;
    }
}
