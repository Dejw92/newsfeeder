package agh.tai.newsfeeder.service.services;

import agh.tai.newsfeeder.event.Event;
import agh.tai.newsfeeder.persistence.interfaces.EventRepository;
import agh.tai.newsfeeder.service.assembler.EventAssembler;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dawid Pawlak.
 */

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Map<String, List<EventDTO>> getGroupComponents() {
        Map<String, List<EventDTO>> map = new HashMap<>();
        List<Event> events = eventRepository.get();
        for(Event event : events) {
            String groupName = event.getGroup().getName();
            EventDTO eventDTO = EventAssembler.convert(event);
            if(!map.containsKey(groupName)) {
                map.put(groupName, Lists.newArrayList(eventDTO));
            } else {
                map.get(groupName).add(eventDTO);
            }
        }
        return map;
    }
}
