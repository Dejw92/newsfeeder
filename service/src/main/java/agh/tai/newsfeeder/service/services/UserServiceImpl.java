package agh.tai.newsfeeder.service.services;

import agh.tai.newsfeeder.User;
import agh.tai.newsfeeder.persistence.interfaces.UserRepository;
import agh.tai.newsfeeder.service.assembler.UserAssembler;
import agh.tai.newsfeeder.service.dtos.CurrentUserDetailsDTO;
import agh.tai.newsfeeder.service.dtos.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Dawid Pawlak.
 */


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Override
    public void save(UserDTO userDTO) {
        User user = UserAssembler.convert(userDTO);
        user.setPassword(passwordEncoder.encodePassword(userDTO.getPassword(),userDTO.getLogin()));
        userRepository.save(user);
    }

    @Override
    public Optional<UserDTO> getUser(String login) {
        UserDTO userDTO = UserAssembler.convert(userRepository.get(login));
        if(userDTO == null) {
            return Optional.empty();
        }
        return Optional.of(userDTO);
    }

    @Override
    public CurrentUserDetailsDTO getCurrentlyLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        List<String> roles = new ArrayList<>();

        for(GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            String role = grantedAuthority.getAuthority();
            roles.add(role);
        }
        return new CurrentUserDetailsDTO(login, roles);
    }
}
