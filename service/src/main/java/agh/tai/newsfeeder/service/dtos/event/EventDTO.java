package agh.tai.newsfeeder.service.dtos.event;

import agh.tai.newsfeeder.BaseEntity;
import agh.tai.newsfeeder.Comment;
import agh.tai.newsfeeder.service.dtos.CommentDTO;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EventDTO {

    private String id;
    private String name;
    private long time;
    private String event_url;
    private String urlname;
    private String status;
    private int yes_rsvp_count;
    private String description;
    private GroupDTO group;
    private VenueDTO venue;

    private List<CommentDTO> comments = new LinkedList<>();

    public EventDTO() {
    }

    public EventDTO( String name, String description ) {
        this.name = name;
        this.description = description;
    }

    public EventDTO( String name, String description, List<CommentDTO> comments ) {
        this(name, description);
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname( String urlname ) {
        this.urlname = urlname;
    }

    public void addComment( CommentDTO commentDTO ) {
        comments.add( commentDTO );
    }

    public String getStatus() {
        return status;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments( List<CommentDTO> comments ) {
        this.comments = comments;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime( long time ) {
        this.time = time;
    }

    public String getEvent_url() {
        return event_url;
    }

    public void setEvent_url( String event_url ) {
        this.event_url = event_url;
    }

    public int getYes_rsvp_count() {
        return yes_rsvp_count;
    }

    public void setYes_rsvp_count( int yes_rsvp_count ) {
        this.yes_rsvp_count = yes_rsvp_count;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public GroupDTO getGroup() {
        return group;
    }

    public void setGroup( GroupDTO group ) {
        this.group = group;
    }

    public VenueDTO getVenue() {
        return venue;
    }

    public void setVenue( VenueDTO venue ) {
        this.venue = venue;
    }
}

