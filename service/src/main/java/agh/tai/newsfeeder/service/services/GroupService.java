package agh.tai.newsfeeder.service.services;

import agh.tai.newsfeeder.service.dtos.event.EventDTO;

import java.util.List;
import java.util.Map;

/**
 * Created by Dawid Pawlak.
 */
public interface GroupService {
    Map<String, List<EventDTO>> getGroupComponents();
}
