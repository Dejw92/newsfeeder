package agh.tai.newsfeeder.service.dtos.event;

import agh.tai.newsfeeder.BaseEntity;

public class GroupDTO {

    private String name;
    private String urlname;

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname( String urlname ) {
        this.urlname = urlname;
    }
}
