package agh.tai.newsfeeder.service.meetup_client;

public class RequestBuilder {

    private final StringBuilder builder = new StringBuilder("http://api.meetup.com/");
    final String apiKey = "key=1515524476c564c4742181125643f7b";

    public String getEventRequest( String eventId ) {
        return builder.append( "/2/event/")
                .append( eventId )
                .append( "?only=id,name,time,event_url,urlname,status,yes_rsvp_count,description,venue.name,venue.country,venue.city,venue.address_1,group.name,group.urlname" )
                .append( "&" + apiKey ).toString();
    }
}
