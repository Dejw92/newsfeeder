package agh.tai.newsfeeder.service.assembler;

import agh.tai.newsfeeder.Comment;
import agh.tai.newsfeeder.service.dtos.CommentDTO;

import java.util.List;
import java.util.stream.Collectors;

public class CommentAssembler {

    public static Comment convert(CommentDTO commentDTO) {
        return new Comment(commentDTO.getId(), commentDTO.getContent(), commentDTO.getAuthor(), commentDTO.getTime());
    }

    public static List<Comment> convert(List<CommentDTO> comments) {
        return comments.stream()
                .map(CommentAssembler::convert)
                .collect(Collectors.toList());
    }
}
