package agh.tai.newsfeeder.service.services;

import agh.tai.newsfeeder.service.dtos.UserDTO;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<UserDTO> userDTO = userService.getUser(login);

        if(!userDTO.isPresent()) throw new UsernameNotFoundException("User with login " + login + " not found");

        return buildUserDetails(userDTO.get());
    }

    private UserDetails buildUserDetails(UserDTO userDTO) {

        List<GrantedAuthority> grantedAuthorities = Lists.newArrayList(new SimpleGrantedAuthority("ROLE_"+userDTO.getRole()));

        return new User(userDTO.getLogin(), userDTO.getPassword(), grantedAuthorities);
    }
}
