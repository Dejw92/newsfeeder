package agh.tai.newsfeeder.service.dtos;

import java.util.Collections;
import java.util.Set;

/**
 * Created by Dawid Pawlak.
 */

public class UserDTO {

    private String login;

    private String role;

    private String firstName;

    private String lastName;

    private String password;


    public UserDTO() {
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
