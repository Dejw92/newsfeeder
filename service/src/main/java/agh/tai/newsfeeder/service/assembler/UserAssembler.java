package agh.tai.newsfeeder.service.assembler;

import agh.tai.newsfeeder.User;
import agh.tai.newsfeeder.service.dtos.UserDTO;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 * Created by Dawid Pawlak.
 */
public class UserAssembler {

    private static final Mapper mapper = new DozerBeanMapper();

    public static User convert(UserDTO userDTO) {
        User user = mapper.map(userDTO, User.class);
        return user;
    }
    public static UserDTO convert(User user) {
        UserDTO userDTO = mapper.map(user, UserDTO.class);
        return userDTO;
    }

}
