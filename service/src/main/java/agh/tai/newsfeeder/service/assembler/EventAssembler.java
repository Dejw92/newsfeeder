package agh.tai.newsfeeder.service.assembler;

import agh.tai.newsfeeder.event.Event;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

public class EventAssembler {

    private static final Mapper mapper = new DozerBeanMapper();

    public static Event convert( EventDTO eventDTO ) {
        Event event = mapper.map( eventDTO, Event.class );
        event.setComments(CommentAssembler.convert(eventDTO.getComments()));
        return event;
    }

    public static EventDTO convert( Event event ) {
        return mapper.map( event, EventDTO.class );
    }
}
