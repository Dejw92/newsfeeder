package agh.tai.newsfeeder.service.meetup_client;

import agh.tai.newsfeeder.event.Event;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.io.IOException;
import java.util.Optional;

public class MeetupClient {

    private final Client client;
    private final RequestBuilder builder;

    public MeetupClient() {
        this.client = Client.create();
        this.builder = new RequestBuilder();
    }

    public Optional<EventDTO> getEvent( String eventId ) throws IOException {

        WebResource resource = client
                .resource( builder.getEventRequest( eventId ) );

        ClientResponse response = resource.accept( "application/json" ).get( ClientResponse.class );

        if( response.getStatus() == 404 )
            return Optional.empty();

        String output = response.getEntity( String.class );
        ObjectMapper mapper = new ObjectMapper( );


        return Optional.of(mapper.readValue( output, EventDTO.class ));
    }
}
