package agh.tai.newsfeeder.service.dtos;


import java.util.List;

public class CurrentUserDetailsDTO {
    private String login;
    private List<String> roles;

    public CurrentUserDetailsDTO(String login, List<String> roles) {
        this.login = login;
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public List<String> getRoles() {
        return roles;
    }
}
