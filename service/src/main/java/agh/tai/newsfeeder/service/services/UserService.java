package agh.tai.newsfeeder.service.services;

import agh.tai.newsfeeder.service.dtos.CurrentUserDetailsDTO;
import agh.tai.newsfeeder.service.dtos.UserDTO;

import java.util.Optional;

/**
 * Created by Dawid Pawlak.
 */
public interface UserService {

    void save(UserDTO userDTO);

    Optional<UserDTO> getUser(String login);

    CurrentUserDetailsDTO getCurrentlyLoggedUser();
}
