package agh.tai.newsfeeder.service.dtos;


import org.joda.time.DateTime;

/**
 * Created by Dawid Pawlak.
 */
public class CommentDTO {

    private String content;

    private DateTime time;

    private String author;

    private String id;

    public CommentDTO() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
