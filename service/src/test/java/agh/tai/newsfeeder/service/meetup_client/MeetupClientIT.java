package agh.tai.newsfeeder.service.meetup_client;

import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class MeetupClientIT {

    private final MeetupClient instance = new MeetupClient();

    @Test
    public void shouldReturnScalaCamp8() throws Exception {
        //when
        final EventDTO event = instance.getEvent( "222158909" ).get();

        //then
        assertThat( event.getName() ).contains( "ScalaCamp #8" );
    }
}
