package agh.tai.newsfeeder.service.assembler;

import agh.tai.newsfeeder.event.Event;
import agh.tai.newsfeeder.service.dtos.event.EventDTO;
import agh.tai.newsfeeder.service.meetup_client.MeetupClient;
import org.junit.Test;
import static org.fest.assertions.Assertions.assertThat;


import java.io.IOException;

public class EventAssemblerTest {

    private final MeetupClient instance = new MeetupClient();
    private EventDTO event;

    public EventAssemblerTest() throws IOException {
        event = instance.getEvent( "222158909" ).get();
    }

    @Test
    public void shouldMapEventDTOtoEventEntity() {
        final Event convert = EventAssembler.convert( event );

        assertThat(convert.getName()).isEqualTo( event.getName() );
    }
}
